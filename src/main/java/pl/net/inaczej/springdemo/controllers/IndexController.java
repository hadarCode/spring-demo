package pl.net.inaczej.springdemo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Łukasz Goliński on 07.03.18.
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String getIndex(){
        return "index";
    }
}
